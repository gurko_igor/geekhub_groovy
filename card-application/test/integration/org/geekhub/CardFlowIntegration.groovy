package org.geekhub

import grails.test.WebFlowTestCase

class CardFlowIntegration extends WebFlowTestCase {

    def cardController = new CardController()

    @Override
    def getFlow() {
        cardController.cardFlow
    }

    void testCardOrder() {
        startFlow()
        assertCurrentStateEquals("createCard")
        cardController.params.name = "test"
        signalEvent("next")

        assertTrue(getFlowScope().card.hasError())
        assertCurrentStateEquals("createCard")

        cardController.params.priceInCents = 10
        signalEvent("next")
        assertCurrentStateEquals("createSet")
    }
}
