import org.geekhub.Role
import org.geekhub.User
import org.geekhub.UserRole

class BootStrap {

    def init = { servletContext ->
        if (Role.list().size() == 0) {
            new Role(authority: "ROLE_ADMIN").save()
            new Role(authority: "ROLE_ANONYMOUS").save()
            new Role(authority: "ROLE_USER").save()
        }

        if(User.list().size() == 0){
            def user = new User(username : 'admin', password: 'admin', enabled: true,
                    email: 'gurko.igor@gmail.com', accountExpired: false , accountLocked: false ,
                    passwordExpired: false).save(flush: true, insert: true)

            /*create the first user role map*/
            UserRole.create user , Role.findByAuthority("ROLE_ADMIN") , true
        }

    }
    def destroy = {
    }
}