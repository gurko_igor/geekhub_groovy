package com.grailsinaction

/**
 * Created by igor on 05.01.14.
 */
class ShippingCommand implements Serializable{

    String address
    String state
    String postcode
    String country
    boolean customShipping
    String shippingType
    String shippingOptions

}
