package com.grailsinaction

/**
 * Created by igor on 05.01.14.
 */
class OrderDetailsCommand implements Serializable{

    int numShirts;
    int numHats;

    boolean isOrderBlank() {
        numShirts == 0 && numHats == 0
    }

    static constraints = {
        numShirts(range: 0..10)
        numHats(range: 0..10)
    }
}
