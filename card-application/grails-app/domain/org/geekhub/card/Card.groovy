package org.geekhub.card

import org.geekhub.Scan
import org.geekhub.SetCard

class Card implements Serializable{

    String name;
    Integer priceInCents;
//    String type; // ??? uses class field in polymorphic association

    static constraints = {
        name(uniq: true, blank: false, nullable: false)
        priceInCents(blank: false, nullable: false, scale: 0, min: 0)
    }

    static belongsTo = [setCard: SetCard]
    static hasOne = [scan: Scan]
}
