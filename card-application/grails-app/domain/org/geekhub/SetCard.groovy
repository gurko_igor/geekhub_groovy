package org.geekhub

import org.geekhub.card.Card

class SetCard implements Serializable{

    String name;
    String slug
    Date releasedAt;

    static hasMany = [ cards: Card ]

    static constraints = {
        name(unique: true, blank: false)
        slug(unique: true, blank: false)
        releasedAt(min: new Date(), blank: false)
    }
}
