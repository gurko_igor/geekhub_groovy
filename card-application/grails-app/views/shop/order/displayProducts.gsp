<%--
  Created by IntelliJ IDEA.
  User: igor
  Date: 05.01.14
  Time: 20:35
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title></title>
    <meta name="layout" content="main">
</head>

<body>

    <g:hasErrors bean="${orderDetails}">
        <div class="errors">
            <g:renderErrors bean="${orderDetails}"/>
        </div>
    </g:hasErrors>

    <g:form action="order">
        Shirts: <g:textField name="numShirts" value="${orderDetails?.numShirts}"/>
        Hats: <g:textField name="numHats" value="${orderDetails?.numHats}"/>
        <g:submitButton name="next" value="Next" />
        <g:submitButton name="cancel" value="Finished Shopping" />
    </g:form>

</body>
</html>