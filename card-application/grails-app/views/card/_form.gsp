
    <div>
        <label for="name" class="control-label">Name<span class="required-indicator">*</span></label>
        <div>
            <g:textField class="form-control" name="name" value="${card?.name}"/>
            <span class="help-inline">${hasErrors(bean: card, field: 'name', 'error')}</span>
        </div>
    </div>

    <div>
        <label for="priceInCents" class="control-label">Price<span class="required-indicator">*</span></label>
        <div>
            <g:textField class="form-control" name="priceInCents" value="${card?.priceInCents}"/>
            <span class="help-inline">${hasErrors(bean: card, field: 'priceInCents', 'error')}</span>
        </div>
    </div>

    %{--<div class="${hasErrors(bean: card, field: 'type', 'error')} required">--}%
        %{--<label for="type" class="control-label">Type<span class="required-indicator">*</span></label>--}%
        %{--<div>--}%
            %{--<g:select class="form-control" name="type" from="${['Normal', 'Wigth']}" required="" value="${card?.type}"/>--}%
            %{--<span class="help-inline">${hasErrors(bean: card, field: 'type', 'error')}</span>--}%
        %{--</div>--}%
    %{--</div>--}%

