<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Create Card</title>
    <meta name="layout" content="main" />
</head>

<body>

<g:hasErrors bean="${set}">
    <div class="alert alert-error">
        <g:renderErrors bean="${set}" as="list" />
    </div>
</g:hasErrors>

<g:form action="card" class="form-horizontal" enctype="multipart/form-data">
    <fieldset class="form"><g:render template="setForm"/></fieldset>
    <div class="form-actions" style="margin-top: 10px;">
        <g:submitButton name="next" class="btn btn-primary"
                        value="${message(code: 'default.button.nextStep.label', default: 'Next')}" />
        <g:submitButton name="previous" class="btn btn-primary"
                        value="${message(code: 'default.button.editCard.label', default: 'Edit Card')}" />
        <g:submitButton name="cancel" class="btn btn-primary"
                        value="${message(code: 'default.button.cancel.label', default: 'Cancel')}" />
    </div>
</g:form>

</body>
</html>