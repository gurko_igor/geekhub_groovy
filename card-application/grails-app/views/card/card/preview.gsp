<%--
  Created by IntelliJ IDEA.
  User: igor
  Date: 05.01.14
  Time: 23:32
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Preview</title>
    <meta name="layout" content="main">
</head>

<body>

<div>Card Name: ${card.name}</div>
<div>Card Price: ${card.priceInCents}</div>
<div>Set Name: ${setCard.name}</div>
<div>Set Slug: ${setCard.slug}</div>
<div>Set Slug: ${setCard.releasedAt}</div>
<g:link action="card" event="save">Save</g:link>

%{--<g:form action="card" class="form-horizontal" enctype="multipart/form-data">--}%
    %{--<g:submitButton name="save" class="btn btn-primary"--}%
                    %{--value="${message(code: 'default.button.save.label', default: 'Save')}" />--}%
    %{--<g:submitButton name="previous" class="btn btn-primary"--}%
                    %{--value="${message(code: 'default.button.editCardSet.label', default: 'Edit Card Set')}" />--}%
    %{--<g:submitButton name="cancel" class="btn btn-primary"--}%
                    %{--value="${message(code: 'default.button.cancel.label', default: 'Cancel')}" />--}%
%{--</g:form>--}%

</body>
</html>