<div>
    <label for="name" class="control-label">Name<span class="required-indicator">*</span></label>
    <div>
        <g:textField class="form-control" name="name" value="${setCard?.name}"/>
        <span class="help-inline">${hasErrors(bean: setCard, field: 'name', 'error')}</span>
    </div>
</div>

<div>
    <label for="slug" class="control-label">Slug<span class="required-indicator">*</span></label>
    <div>
        <g:textField class="form-control" name="slug" value="${setCard?.slug}"/>
        <span class="help-inline">${hasErrors(bean: setCard, field: 'slug', 'error')}</span>
    </div>
</div>

<div>
    <label for="releasedAt" class="control-label">Released Date<span class="required-indicator">*</span></label>
    <div>
        <g:datePicker class="form-control" name="releasedAt" value="${setCard?.releasedAt}" precision="day"/>
        <span class="help-inline">${hasErrors(bean: setCard, field: 'releasedAt', 'error')}</span>
    </div>
</div>

