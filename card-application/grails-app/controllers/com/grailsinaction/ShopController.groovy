package com.grailsinaction

import com.sun.tools.doclets.formats.html.resources.standard

class ShopController {

//    def creditCardService

    def index() {
        redirect(action: "order")
    }

    def orderFlow = {
        displayProducts {
            on("next") { OrderDetailsCommand odc ->

                if (odc.hasErrors() || odc.isOrderBlank()) {
                    flow.orderDetails = odc
                    return error()
                }

                [orderDetails: odc, orderStartDate: new Date()]
            }.to("enterAddress")

            on("cancel").to("finish")
        }

        enterAddress {
            on("next") { ShippingCommand sc ->
                conversation.sc = sc

                if (sc.hasErrors()) {
                    return error()
                }

            }.to("validateCard")

            on("previous").to("displayProducts")
            on("cancel").to("finish")
        }

        enterPayment {
            on("next") { PaymentCommand pc ->
                flow.pc = pc

                if (pc.hasErrors()) {
                    return error()
                }

            }.to("validateCard")

            on("previous").to("enterAddress")
            on("cancel").to("finish")
        }

        validateCard {
            action {
                def validCard = false //creditCardService.checkCard(flow.pc.cardNumber, flow.pc.name,flow.pc.expiry)

                if (validCard) {
                    valid()
                }
                else {
                    flow.pc.errors.rejectValue("CardNumber", "card.failed.validation", "This credit card is dodgy")
                    invalid()
                }
            }

            on("valid").to("orderComplete")
            on("invalid").to("enterPayment")
        }

        checkShipping {
            action {
                if (conversation.sc.customShipping) {
                    custom()
                }
                else {
                    standart()
                }
            }

            on("custom").to("customShipping")
            on("standart").to("enterPayment")
        }

        customShipping {
            subflow(customShippingFlow)
            on("goBack").to("enterAddress")
            on("standartShipping") {
                conversation.sc.customShipping = false
            }.to("enterPayment")
            on("customShipping").to("enterPayment")
        }

        finish {
            redirect(controller: "home", action: "index")
        }
    }

    def customShippingFlow = {
        selectShippingType {
            on("next") {
                conversation.sc.shippingType = params.shippingType
            }.to("selectShippingOptions")
            on("standartShipping").to("standartShipping")
            on("previous").to("goBack")
        }

        selectShippingOptions {
            on("previous").to("selectShippingType")
            on("next") {
                conversation.sc.shippingOptions = params.shippingOptions
            }.to("customShipping")
        }

        customShipping()
        standardShipping()
        goBack()
    }
}
