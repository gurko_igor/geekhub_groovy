package org.geekhub

import org.geekhub.card.Card

class CardController {

    def index() {
    }

    def create() {
        redirect action: "card"
    }

    def list() {

    }

    def cardFlow = {
        createCard {
            on("next") {Card card ->

                flow.card = card

                if (card.hasErrors()) {
                    return error()
                }

            }.to("createSet")

            on("cancel").to("finish")
        }

        createSet {
            on("next") { SetCard setCard ->

                flow.setCard = setCard

                if (setCard.hasErrors()) {
                    return error()
                }

            }.to("preview")

            on("previous").to("createCard")
            on("cancel").to("finish")
        }

        preview {
            on("save") {

                System.out.println('----Preview----')
                System.out.println(flow.card)
                System.out.println(flow.setCard)

                if (flow.card.hasErrors() || flow.setCard.hasErrors()) {
                    return error();
                }
                else {
                    flow.card.save()
                    flow.setCard.save()
                }
            }.to("finish")

            on("previous").to("createSet")
            on("cancel").to("finish")
        }

        finish {
            redirect(controller: "card", action: "index")
        }
    }
}
