package org.geekhub.card

import org.geekhub.Scan
import org.geekhub.SetCard

class Card {

    String name;
    Integer priceInCents;
    String type; // ??? uses class field in polymorphic association

    static constraints = {
        name(uniq: true)
    }

    static belongsTo = [setCard: SetCard]
    static hasOne = [scan: Scan]
}
