package org.geekhub

import org.geekhub.card.Card

class SetCard {

    String name;
    String slug
    Date releasedAt;

    static hasMany = [ cards: Card ]

    static constraints = {
    }
}
