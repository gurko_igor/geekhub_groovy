package test

/**
 * Created with IntelliJ IDEA.
 * User: igor
 * Date: 23.11.13
 * Time: 13:09
 * To change this template use File | Settings | File Templates.
 */

class HelloSpock extends spock.lang.Specification {
    def "length of Spock's and his friends' names"() {
        expect:
        name.size() == length

        where:
        name     | length
        "Spock"  | 5
        "Kirk"   | 4
        "Scotty" | 6
    }
}
