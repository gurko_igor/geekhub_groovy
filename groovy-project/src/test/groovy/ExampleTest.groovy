package test

import model.ArithmeticModel

class ExampleTest extends GroovyTestCase {

    void testStringEquals() {
        String str1 = "string_example_1"
        String str2 = "string_example_1"

        assertEquals(str1, str2)
        assert str1 == str2
    }

    void testShouldArrayIncludeObject() {
        def collections = ["element_1", "element_2"]

        assert collections.contains("element_1")
        assertFalse(collections.contains("element_3"))
    }

    void testCalculateFactorial() {
        assert ArithmeticModel.factorialRecursive(4) == 1*2*3*4
        assert ArithmeticModel.factorial(5) == 1*2*3*4*5
    }

}
