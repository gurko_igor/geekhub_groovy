package model

/**
 * Created with IntelliJ IDEA.
 * User: igor
 * Date: 11.10.13
 * Time: 00:01
 * To change this template use File | Settings | File Templates.
 */
class ArithmeticModel {

    static factorialRecursive(int value) {
        if (value == 1)
            1
        else
            value * factorialRecursive(value - 1)
    }

    static factorial(int n) {
        def res = 1

//        for (int i = 1; i <= n; i++) {
//            res *= i
//        }

        (1..n).collect { el -> res *= el }

        res
    }

}
