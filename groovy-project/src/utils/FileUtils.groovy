package utils

def folderPath = args ? args[0] : "./tmp"
def currentFolder = new File(folderPath)

class ContentUtil {
    private currentFolder
    private Random random = new Random();

    ContentUtil(currentFolder) {
        this.currentFolder = currentFolder
    }

    def generateTestContent() {
        setupFolders()
        setupFiles(getAllFolderPaths())
    }

    def setupFolders() {
        new File("./tmp/first/1").mkdirs()
        new File("./tmp/first/2").mkdirs()
        new File("./tmp/second/1/1").mkdirs()
    }

    def setupFiles(folderPaths) {
        Integer maxRandomValue = folderPaths.size() - 1

        (1..3).each {
            def path = folderPaths[random.nextInt(maxRandomValue)]
            def file = new File(path + "/a${it}.${getFileType()}")
            file.write("asdjhsad sajdhaskjh das jash dklajh")
        }

        (1..3).each {
            def path = folderPaths[random.nextInt(maxRandomValue)]
            def file = new File(path + "/1${it}.${getFileType()}")
            file.write("some text for '1' files")
        }

        (1..3).each {
            def path = folderPaths[random.nextInt(maxRandomValue)]
            def file = new File(path + "/d${it}.${getFileType()}")
            file.write("some text for 'd' files")
        }

        (1..3).each {
            def path = folderPaths[random.nextInt(maxRandomValue)]
            def file = new File(path + "/same${it}.${getFileType()}")
            file.write("other files")
        }
    }

    def getFileType() {
        def fileTypes = ["html", "rb", "txt", "js"]
        fileTypes[random.nextInt(fileTypes.size() - 1)]
    }

    def getAllFolderPaths() {
        def folderListPaths = []
        currentFolder.traverse { file ->
            if (file.isDirectory()) {
                folderListPaths.add(file.path)
            }
        }
        folderListPaths
    }

    def processAFile(File file) {
        def countChars = file.getText().count("a")
        file.append("Count the number of occurrences of a substring: ${countChars}")
        fileProcessed(file)

    }

    def process1File(File file) {
        def matrix = new Matrix()
        def array = matrix.randomGenerateArray()
        def sum = matrix.calculateMainDiagonals()
        def separator = System.getProperty("line.separator")

        StringBuilder newFileContent = new StringBuilder()
//        for (i in array.size()) {
//            newFileContent.append(array[i].toString())
//            newFileContent.append(separator)
//        }

        newFileContent.append(array.toString())
        newFileContent.append(separator)
        newFileContent.append("Sum main diagonals: ${sum}")

        file.setText(newFileContent.toString())
        fileProcessed(file)
    }

    def processDFile(File file) {
        //TODO use regular expression
//        def pattern = ~/.4+/
//        println(pattern.matcher(file.getName()).matches())
        if (!file.getName().contains("done")) {
            file.setText("CurrentTime is: " + new Date().format("yyyy-MM-dd HH-mm-ss"))
            fileProcessed(file)
        }
    }

    def fileProcessed(File file) {
        def newName = [file.parentFile.getPath(), "done_${file.getName()}"].join("/")
        file.renameTo(new File(newName))
    }
}

class Matrix {

    def SIZE = 4
    def ROW = 4

    def array = new Object[SIZE][ROW]

    def randomGenerateArray() {
        def random = new Random();
        array.each { it ->
            it.eachWithIndex { e, index -> it.putAt(index, random.nextInt(9) ) }
        }
    }

    def calculateMainDiagonals() {
        SumDiagonal(0..SIZE-1)
    }

    def SumDiagonal(range) {
        range.sum { i ->
            array[i][i] + array[i][(i+1)*-1]
        }
    }
}

ContentUtil content = new ContentUtil(currentFolder)

if (!currentFolder.list()) {
    content.generateTestContent()
}

currentFolder.traverse { file ->
    if (file.isFile()) {
        println("Process file: ${file.getName()}")

        switch (file.getName()[0]) {
            case "a": content.processAFile(file); break;
            case "1": content.process1File(file); break;
            case "d": content.processDFile(file); break;
        }
    }
}

println("finished")